﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace GiS_Commands {
    class GIS_LF_API {
        // Get Library Version
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_LibVersion();

        // Get Names of all USB Devices connected, only valid devices are listed
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_GetUSBDeviceNames(sbyte[] NamenListe, int nBufferSize);

        // Get Names of all USB Devices connected, all devices from GiS are listed
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_GetUSBDeviceNamesEx(sbyte[] NamenListe, int nBufferSize);

        // Get Names of all COM Ports available, not tested if there is a device at the port connected.
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_GetCOMDeviceNames(sbyte[] NamenListe, int nBufferSize);

        // Get Names of all valid LAN devices found in same subnet
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_GetLanDeviceNames(sbyte[] NamenListe, int nBufferSize);

        // Get Names of all LAN devices found even in other subnets
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_GetLanDeviceNamesEx(sbyte[] Buffer, int nBufferSize);

        // Change IP Address of LAN device
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_ChangeLanIPAddress(sbyte[] OldAddress, sbyte[] NewAddress, sbyte[] IPMask);

        // Check if specific LAN device is available
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_IsLanDeviceAvailable(sbyte[] Address);

        // Open LAN Device for configuration
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_LanConfigOpen(sbyte[] strName);

        // Set Configuration Record of LAN device, use with care!
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_LanConfigGet(int nHandle, int RecordNr, Byte[] pData, int Len);

        // Get Configuration Record of LAN device
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_LanConfigSet(int nHandle, int RecordNr, Byte[] pData, int Len);

        // Close LAN device configuration.
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_LanConfigClose(int nHandle, int Reset);

        // Read Configuration of USB HID device
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_HIDConfigRead(int PortHandle, Byte[] pSendBuf, int SendBufLen, Byte[] pRecvBuf, int RecvBufLen);
        // Write Configuration to USB HID device
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_HIDConfigWrite(int PortHandle, Byte[] pSendBuf, int SendBufLen);

        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Open(sbyte[] Name, int Baudrate, int ParityMode, int Timeout);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Close(int handle);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_KeepAppActive(int handle, int bActive);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_SetReaderAdresse(int handle, int Adresse);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_GetDeviceVersion(int handle, sbyte[] pDevVer, int VerLen, sbyte[] pDevName, int NameLen);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_IsUSB(int handle);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_GetFilter(int handle, int TTyp, Byte[] pParam, int nParamLen);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_SetFilter(int handle, int TTyp, Byte[] pParam, int nParamLen);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_SetBaudrate(int handle, int Baudrate, int ParityMode);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_SetReaderMode(int handle, int mode);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_SetRF(int handle, int OnOff);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_GetLastError(int handle);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_SetIO(int handle, int Maske, int Daten);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_ReadIO(int handle, int[] pDaten);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_TestIO(int handle, int[] pTestwert);

        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_ClearLCD(int handle);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_SetLCDFont(int handle, int FontNr);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_SetLCDText(int handle, int StartLine, sbyte[] strText);

        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_ReadSerialNumber(int handle, Byte[] pSerial, int Buflen);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_SetConfig(int handle, Byte[] pConfig, int ConfigLen);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_GetConfig(int handle, Byte[] pConfig, int ConfigLen);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_SetDefault(int handle);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_SetAnswerMode(int handle, int AnswerMode);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_GetAnswerMode(int handle);

        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_ReadMagnetCard(int handle, Byte[] pBuffer, int BufLen);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_ClearMagnetCard(int handle);

        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_SetCommunicationMode(int handle, int Mode);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_RawWrite(int handle, Byte[] pBuffer, int BufLen);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_RawRead(int handle, Byte[] pBuffer, int BufLen);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Transfer(int PortHandle, int Cmd, Byte[] pSendBuf, int SendBufLen, Byte[] pRecvBuf, int RecvBufLen);

        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Read(int handle, int TransponderTyp, int WortBlockPageNr, Byte[] pBuffer, int BufLen);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Write(int handle, int TransponderTyp, int WortBlockPageNr, Byte[] pBuffer, int BufLen, int Lock);

        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_GetTagType(int handle, int TTypExpected);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_GetTagAnswerMode(int handle, int TTyp, int AnswerModeExpected);

        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Write_Unique(int handle, int TransponderTyp, Byte[] pBuffer, int BufLen, int Lock);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Write_UniqueXL(int handle, int TransponderTyp, Byte[] pBuffer, int BufLen, int Lock);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Write_GiS16Bit(int handle, int TransponderTyp, Byte[] pBuffer, int BufLen, int Lock);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Write_GiS12Bit(int handle, int TransponderTyp, Byte[] pBuffer, int BufLen, int Lock);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Write_GiS72Bit(int handle, int TransponderTyp, Byte[] pBuffer, int BufLen, int Lock);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Write_FSK26Bit(int handle, int TransponderTyp, uint FacilityCode, uint CardNr, int Lock);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Write_FSK34Bit(int handle, int TransponderTyp, uint FacilityCode, uint CardNr, int Lock);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Write_FSK35Bit(int handle, int TransponderTyp, uint FacilityCode, uint CardNr, int Lock);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Write_FSK36Bit(int handle, int TransponderTyp, uint FacilityCode, uint CardNr, uint FixedField, int Lock);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Write_FSK37Bit(int handle, int TransponderTyp, uint FacilityCode, uint CardNr, int Lock);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Write_FSK37BitHuge(int handle, int TransponderTyp, Byte[] pCode, int CodeLen, int Lock);


        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Read_TTFData(int handle, int AnswerMode, Byte[] pBuffer, int BufLen);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Read_Unique(int handle, Byte[] pBuffer, int BufLen);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Read_UniqueXL(int handle, Byte[] pBuffer, int BufLen);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Read_GiS16Bit(int handle, Byte[] pBuffer, int BufLen);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Read_GiS12Bit(int handle, Byte[] pBuffer, int BufLen);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Read_GiS72Bit(int handle, Byte[] pBuffer, int BufLen);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Read_FSK26Bit(int handle, uint[] pFacilityCode, uint[] pCardNr);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Read_FSK34Bit(int handle, uint[] pFacilityCode, uint[] pCardNr);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Read_FSK35Bit(int handle, uint[] pFacilityCode, uint[] pCardNr);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Read_FSK36Bit(int handle, uint[] pFacilityCode, uint[] pCardNr, uint[] pFixedField);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Read_FSK37Bit(int handle, uint[] pFacilityCode, uint[] pCardNr);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Read_FSK37BitHuge(int handle, Byte[] pCode, int CodeLen);

        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_ReadFormattedData(int handle, int TTyp, int ReadType, Byte[] pBuffer, int BufLen);

        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_ResetTransponder(int handle, int TransponderTyp);

        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_UID_Request(int PortHandle, int TransponderTyp, Byte[] pUID, int BufLen);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Select(int PortHandle, int TransponderTyp, Byte[] pSelData, int SelDataLen, Byte[] pConfig, int ConfigLen);

        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_WakeUp(int handle, int TransponderTyp, Byte[] pPasswort, int PasswortLen);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Login(int handle, int TransponderTyp, Byte[] pPW, int nPWLen);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_WritePW(int handle, int TransponderTyp, Byte[] pOldPW, int OldPWLen, Byte[] pNewPW, int NewPWLen);

        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Disable(int handle, int TransponderTyp);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Lock(int handle, int TransponderTyp, int BlockNr);

        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_GetTagInfo(int PortHandle, int TransponderTyp, Byte[] pSysInfo, int SysInfoLen);

        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_WriteParam(int handle, Byte[] pBuffer, int BufLen);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_ReadParam(int handle, Byte[] pBuffer, int BufLen);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_WritePrefix(int handle, Byte[] pBuffer, int BufLen);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_ReadPrefix(int handle, Byte[] pBuffer, int BufLen);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_WriteSuffix(int handle, Byte[] pBuffer, int BufLen);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_ReadSuffix(int handle, Byte[] pBuffer, int BufLen);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_WriteTermix(int handle, Byte[] pBuffer, int BufLen);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_ReadTermix(int handle, Byte[] pBuffer, int BufLen);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_WritePostcode(int handle, Byte[] pBuffer, int BufLen);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_ReadPostcode(int handle, Byte[] pBuffer, int BufLen);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_WriteReadModeParam(int handle, Byte[] pBuffer, int BufLen);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_ReadReadModeParam(int handle, Byte[] pBuffer, int BufLen);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_WriteHitag2Key(int handle, Byte[] pBuffer, int BufLen);

        // Functions for ISO11784 (Animal code and Waste management code)
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Read_FDXA(int handle, Byte[] pIdNumber, int pIdLen);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Write_FDXA(int handle, int TTyp, Byte[] pFactoryID, Byte[] pIdNumber, int pIdLen, int Lock);

        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Read_FDXB(int handle, int[] pCountryCode, Byte[] pBuffer, int BufLen, int[] pAnimalFlag, int[] pDatenblockFlag, int[] pReserved, int[] pExtension);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Write_FDXB(int handle, int TransponderTyp, Byte[] pFactoryID, int CountryCode, Byte[] pIDNummer, int IDNummerLen, int AnimalFlag, int DatenblockFlag, int Reserved, int Extension, int Lock);

        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Read_HDX(int handle, int[] pCountryCode, Byte[] pBuffer, int BufLen, int[] pAnimalFlag, int[] pDatenblockFlag, int[] pReserved, int[] pExtension);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Write_HDX(int handle, int TransponderTyp, Byte[] pFactoryID, int CountryCode, Byte[] pIDNummer, int IDNummerLen, int AnimalFlag, int DatenblockFlag, int Reserved, int Extension, int Lock);

        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Read_EN14803(int handle, int[] pManufacturer, int[] pSerialNumber);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Read_EN14803HDX(int handle, int[] pManufacturer, int[] pSerialNumber);
        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_Write_EN14803(int handle, int TransponderTyp, Byte[] pFactoryID, int Manufacturer, int SerialNumber, int Lock);

        [DllImport("TS_LF_SDK.dll")]
        public static extern int TSLF_IsWriteFinished(int handle, Byte[] pFactoryID);

        public GIS_LF_API() {
        }
    }
}
