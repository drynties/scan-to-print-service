﻿using AIOUSBNet;
using GiS_Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Xml;

namespace Scan_To_Print_Service {
    public partial class Main : ServiceBase {

        private static UInt32 DeviceIndex;
        private static System.Timers.Timer aTimer = new System.Timers.Timer();

        //GiS Variables
        private static int intHandle;
        private static sbyte[] NameList = new sbyte[255];

        //---Settings.xml Variables---
        private static string LFReaderPort;
        private static int hornDuration;
        private static string suffixCharacters;

        //Serial Port Variables
        private static string printerPort;
        private static int baudRate;
        private static Parity parityP;
        private static int dataBits;
        private static StopBits stopBitsS;

        private static List<string> previousBitValues = new List<string>();
        private static string path = "c:\\eAgile\\RFID Scan-To-Print";
        private static string previousValue = "";
        private static Boolean failState = false;
        public Main() {
            InitializeComponent();
        }
        internal void Start() {
            OnStart(null);
        }

        protected override void OnStart(string[] args) {
            //Application Code
            System.IO.Directory.CreateDirectory(path); //Create error log directory

            loadSettingsFile(); //Load variables from settings.xml file
            GisConnect(); //Connect to Gis Device

            //Check input status every 300ms
            Timer aTimer = new System.Timers.Timer();
            aTimer.Elapsed += new ElapsedEventHandler(checkInputEvent);
            aTimer.Interval = 300;
            aTimer.Enabled = true;
        }

        protected override void OnStop() {
            setAllOutputsLow();
        }
        private static void checkInputEvent(object source, ElapsedEventArgs e) {

            UInt32 result;
            aTimer.Enabled = false;
            byte[] bits = new byte[4];

            result = AIOUSB.DIO_ReadAll(DeviceIndex, bits);

            string txtCheck = ByteArrayToString(bits);

            /*
             * Code below will only run when there is a change in the input values
             *     from any value, to that representing both triggers down. This 
             *     eliminates to the need to handle timing / memory access exceptions.
             *     
             * failState is a boolean variable used to hold the checkInputEvent
             *     until the failure process has completed.  
             */
            if (!txtCheck.Equals(previousValue) && (!failState)) {
                previousValue = txtCheck;
                if (txtCheck.Substring(5, 1).Equals("3")) { //----Process 2 
                    setAllOutputsLow();
                    string tagData = readTag(); // Process 3
                    if (!tagData.Equals("Fail")) {
                        goodTag(tagData); //----Process 4
                    } else { badTag(); } //----Process 6                 
                    ResetDevice(); // GiS device reset
                }
                aTimer.Enabled = true;
            }
        }
        private static void goodTag(string tagData) {
            AIOUSB.DIO_Write1(DeviceIndex, 1, 0); //Output 1 high                                                        
            sendData(tagData + suffixCharacters); //----Process 5

        }
        private static void badTag() {
            failState = true; //Used to keep application out until process 6 is complete
            AIOUSB.DIO_Write1(DeviceIndex, 2, 0); //Output 2 high
            AIOUSB.DIO_Write1(DeviceIndex, 3, 0); //Output 3 high
            System.Threading.Thread.Sleep(hornDuration); //Sleep for x seconds
            AIOUSB.DIO_Write1(DeviceIndex, 3, 1); //Output 3 low
            failState = false;
        }
        private static string ByteArrayToString(byte[] ba) {
            string hex = BitConverter.ToString(ba);
            return hex.Replace("-", "");
        }
        private static void loadSettingsFile() {

            try {
                XmlDocument settings = new XmlDocument();

                settings.Load("settings.xml");

                XmlNodeList elemList = settings.GetElementsByTagName("variable");
                LFReaderPort = elemList[0].InnerXml;
                printerPort = elemList[1].InnerXml;
                hornDuration = Int32.Parse(elemList[2].InnerXml);
                suffixCharacters = elemList[3].InnerXml;
                baudRate = Int32.Parse(elemList[4].InnerXml);
                string parityTest = (elemList[5].InnerXml);
                dataBits = Int32.Parse(elemList[6].InnerXml);
                string stopBits = (elemList[7].InnerXml);

                parityP = (Parity) Enum.Parse(typeof(Parity), parityTest, true);
                stopBitsS = (StopBits) Enum.Parse(typeof(StopBits), stopBits, true);
            } catch (Exception ex) {
                WriteLog(ex.ToString(), System.Reflection.MethodInfo.GetCurrentMethod().Name);
                Environment.Exit(0); //exit application if file not found
            }

        }
        private static void GisConnect() {

            int intResponse, intI;
            string conStringGiS, strTemp;

            try {

                conStringGiS = "\\\\.\\" + LFReaderPort + " [GiS Virtual COM]";
                intI = 0;

                foreach (char c in conStringGiS) {
                    strTemp = Convert.ToInt32(c).ToString();
                    NameList[intI] = Convert.ToSByte(strTemp);
                    intI++;
                }

                intHandle = GIS_LF_API.TSLF_Open(NameList, 19200, 0, 1000);
                if (intHandle > 0) {
                    intResponse = GIS_LF_API.TSLF_SetIO(intHandle, 224, 0);
                    if (intResponse == 0) {
                        intResponse = GIS_LF_API.TSLF_SetIO(intHandle, 224, 64);  //Green LED
                    }
                }

            } catch (Exception ex) {
                WriteLog(ex.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
        }
        private static string readTag() {
            byte[] bytRead = new byte[4];
            int intResponse;
            string strWrite;
            List<string> tagList = new List<string>();
            List<string> checkSumList = new List<string>();

            for (int i = 0; i < 3; i++) {
                intResponse = GIS_LF_API.TSLF_Read(intHandle, 9, 1, bytRead, 4);
                if (intResponse > 0) {
                    strWrite = FormatResult(bytRead);
                    tagList.Add(strWrite);
                    checkSumList.Add(CalculateChecksum(strWrite));
                } else { break; }//No tag found. Break.
            }
            if (tagList.Count == 3) {
                if (checkList(tagList) && checkList(checkSumList)) { //check if any values differ from others
                    return tagList[0]; //good tags with same values & checksums
                } else { return "Fail"; }
            } else { return "Fail"; }

        }
        //Function used to check if all items in list are equal
        private static bool checkList(List<string> elements) {
            if (elements.Any(o => o != elements[0])) {
                return false;
            } else { return true; }
        }
        private static string CalculateChecksum(string dataToCalculate) {
            byte[] byteToCalculate = Encoding.ASCII.GetBytes(dataToCalculate);
            int checksum = 0;
            foreach (byte chData in byteToCalculate) {
                checksum += chData;
            }
            checksum &= 0xff;
            return checksum.ToString("X2");
        }

        private static string FormatResult(byte[] arr) {
            string strResult;
            Array.Reverse(arr);
            strResult = BitConverter.ToString(arr);
            strResult = strResult.Replace("-", "");
            return strResult;
        }
        private static void ResetDevice() {
            int intResponse;
            intResponse = GIS_LF_API.TSLF_ResetTransponder(intHandle, 9);

        }
        public static void setAllOutputsLow() {
            UInt32 bits = 0;
            UInt32.TryParse("FFFFFFFF", NumberStyles.HexNumber, null as IFormatProvider, out bits);
            AIOUSB.DIO_WriteAll(DeviceIndex, ref bits);
        }
        public static void sendData(string data) {

            try {
                SerialPort port = new SerialPort(
                            printerPort, baudRate, parityP, dataBits, stopBitsS);
                port.Open(); // Open the port for communications
                byte[] bytes = Encoding.ASCII.GetBytes(data); // Write bytes
                port.Write(bytes, 0, bytes.Length);
                System.Threading.Thread.Sleep(250); //possibly not required

                port.Close(); // Close the port
            } catch (Exception ex) {
                WriteLog(ex.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name); //Sends the caught error within function x
            }

        }
        private static void WriteLog(string logMessage, string function) {
            using (StreamWriter w = File.AppendText("c:\\eAgile\\RFID Scan-To-Print\\log.txt")) {
                w.WriteLine("## " + function + " Error ##");
                w.WriteLine(DateTime.Now);
                w.WriteLine(logMessage);
                w.WriteLine();
            }
        }

    }
}
